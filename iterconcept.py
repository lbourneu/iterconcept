"""Iterative computation of formal concepts using ASP"""

import clingo
import argparse
from collections import defaultdict
from itertools import starmap
from operator import itemgetter


ASP_INTENT_GENERATION = """
ext(k,O) :- rel(O,_) ; rel(O,A): int(k,A).
int(k,A) :- rel(k,A) ; rel(O,A): ext(k,O).
:- not int(_,_).
:- not ext(_,_).
"""
ASP_CONCEPT_GENERATION = """
ext(O) :- rel(O,_) ; rel(O,A): int(A).
"""


def cli_options():
    "Return parsed cli options"
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("relations", nargs="?", type=str, default="relations.lp",
                        help="ASP encoding containing rel/2 atoms")
    parser.add_argument("--asp-output", "-asp", action="store_true",
                        help="Outputs the concept as ASP atoms")
    return parser.parse_args()


def prettyfied_concept(concept: (set, set)) -> str:
    reprset = lambda elems: "{" + ",".join(map(str, elems)) + "}"
    return " × ".join(map(reprset, concept))


def concept_as_asp_lines(idx: int, concept: (set, set)) -> [str]:
    "Yield the ASP encoding of given concept"
    yield f"concept({idx})."
    objs, atts = concept
    yield " ".join(f"ext({idx},{obj})." for obj in objs)
    yield " ".join(f"int({idx},{att})." for att in atts)


def concept_as_asp(idx: int, concept: (set, set)) -> str:
    "Return the ASP encoding of given concept"
    return "\n".join(concept_as_asp_lines(idx, concept))


def context_to_asp(context: (str, tuple)) -> [str]:
    "Yield lines of ASP encoding of given context, iterable of (object, [attributes])"
    for object, attrs in context:
        attrs_repr = ";".join(map(str, attrs))
        yield f"rel({str(object)},({attrs_repr}))."


def extract_relations_iteratively(fname: str) -> [str]:
    "Yield subsets of increasing size of relations found in given filename, in ASP"
    ctl = clingo.Control()  # new controller over a clingo instance
    ctl.configuration.solve.models = 0
    ctl.load(fname)
    ctl.ground([("base", [])])
    context = defaultdict(set)  # object: {attributes}
    with ctl.solve(yield_=True) as handle:
        # extract the full context as a dict
        for model in handle:
            for atom in model.symbols(atoms=True):
                if atom.name == "rel" and len(atom.arguments) == 2:
                    obj, att = atom.arguments
                    context[obj].add(att)
    # order the context based on its objects, yield the context subsets
    ordered_context = tuple(sorted(tuple(context.items()), key=itemgetter(0)))
    for idx in range(len(ordered_context)):
        yield "\n".join(context_to_asp(ordered_context[: idx + 1]))


def intents_from_relations(step: int, relations: str) -> [frozenset]:
    "Yield intents as frozenset found in given step/relations"
    ctl = clingo.Control()
    ctl.configuration.solve.models = 0
    ctl.add("base", [], relations)
    ctl.add("conceptgen", ["k"], ASP_INTENT_GENERATION)
    ctl.ground([("base", []), ("conceptgen", [step])])
    with ctl.solve(yield_=True) as handle:
        for model in handle:
            intent = set()
            for atom in model.symbols(atoms=True):
                if atom.name == "int" and len(atom.arguments) == 2:
                    intstep, att = atom.arguments
                    intent.add(att)
                    assert intstep.number == step, f"ASP yielded a int/2 atom with step={intstep} instead of {step}"
            yield frozenset(intent)
            # print('MODEL:', intent, model)


def concept_from_intent(intent: set, relations: str) -> (set, set):
    ctl = clingo.Control()
    ctl.configuration.solve.models = 0
    # print('INTENT:', intent)
    intent_as_asp = " ".join(f"int({str(att)})." for att in intent)
    # print('      :', intent_as_asp)
    asp_source = relations + "\n" + intent_as_asp + "\n" + ASP_CONCEPT_GENERATION
    # print(asp_source)
    ctl.add("base", [], asp_source)
    ctl.ground([("base", [])])
    with ctl.solve(yield_=True) as handle:
        for idx, model in enumerate(handle):
            extent = set(
                atom.arguments[0]
                for atom in model.symbols(atoms=True)
                if atom.name == "ext" and len(atom.arguments) == 1
            )
            yield extent, intent
            if idx > 0:  # multiple models ?!?!
                print(f"WARNING multiple models were found for the generation"
                      " of extent associated with intent '{str(intent)}'")


def run(relations_file: str = "relations.lp"):
    """Perform the iterative extraction of intents,
    the derivation of their extent, and yield the subsequent concepts

    """
    contexts = extract_relations_iteratively(relations_file)
    intents = set()  # set of sets describing the extents  (doublons removed)
    for step, asp_context in enumerate(contexts):
        intents |= set(intents_from_relations(step, asp_context))
    # now, build the final extents  (NB: asp_context is now maximal)
    for intent in intents:
        yield from concept_from_intent(intent, asp_context)


if __name__ == "__main__":
    options = cli_options()
    concepts = run(options.relations)
    if options.asp_output:
        print("\n".join(starmap(concept_as_asp, enumerate(concepts))))
    else:
        print("\n".join(map(prettyfied_concept, concepts)))
